<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'axl_master_pegawai';

    protected $primaryKey = 'userid';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ["userid", "nip", "karpeg", "gelar_depan",
        "gelar_belakang", "nama_lengkap", "tempat_lahir", "tanggal_lahir",
        "jenis_kelamin", "id_agama", "id_jenjangpendidikan", "id_golongandarah",
        "id_jabatan", "id_pangkatgol", "id_unitkerja", "no_ktp", "alamat_rumah",
        "no_hp", "no_npwp", "no_askes", "email", "photo", "flag_login"];

}
