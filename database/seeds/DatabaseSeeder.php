<?php

use Illuminate\Database\Seeder;
use App\Employee;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker= Faker\Factory::create();
        foreach (range(1, 100) as $i) {
            Employee::create([
                "nip"                  => $faker->randomNumber(9) . $faker->randomNumber(9),
                "nama_lengkap"         => $faker->name,
                "tempat_lahir"         => $faker->state,
                "tanggal_lahir"        => $faker->date('Y-m-d', '-20 years'),
                "jenis_kelamin"        => $faker->randomElement(['L','P']),
                "id_agama"             => 1,
                "id_jenjangpendidikan" => $faker->numberBetween(4,8),
                "id_golongandarah"     => $faker->numberBetween(1,4),
                "id_jabatan"           => $faker->numberBetween(4, 30),
                "id_pangkatgol"        => $faker->numberBetween(5,14),
                "id_unitkerja"         => $faker->randomElement(["AA", "AC", "AB"]),
                "no_ktp"               => $faker->randomNumber(8) . $faker->randomNumber(9),
                "alamat_rumah"         => $faker->address,
                "no_hp"                => $faker->phoneNumber,
                "no_npwp"              => $faker->randomNumber(6) . $faker->randomNumber(9),
                "no_askes"             => $faker->randomNumber(9),
                "email"                => $faker->unique()->email,
                "photo"                => $faker->md5 . ".jpg",
                "flag_login"           => $faker->randomElement([1,0])
            ]);
        }
    }
}
