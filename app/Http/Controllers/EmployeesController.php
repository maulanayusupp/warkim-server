<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Employee;
use Illuminate\Support\Facades\Validator;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validSort = ['userid', 'nip', 'nama_lengkap', 'email'];
        $sort = $request->has('sort') && in_array($request->get('sort'), $validSort) ? $request->get('sort') : 'userid';
        $order = $request->has('order') && in_array($request->get('order'), ['desc', 'asc']) ? $request->get('order') : 'desc';
        $q = $request->get("q");
        return Employee::where('nama_lengkap', 'LIKE', '%' . $q . '%')
            ->orWhere('email', 'LIKE', '%' . $q . '%')
            ->orderBy($sort, $order)
            ->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // sample validasi, silahkan ditambah
        $validator = Validator::make($request->all(), [
            'nip'          => 'required|unique:axl_master_pegawai',
            'nama_lengkap' => 'required',
            'email'        => 'required|unique:axl_master_pegawai',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $employee = Employee::create($request->toArray());
        return response()->json($employee->toArray(), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Employee::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::findOrFail($id);

        // sample validasi, silahkan ditambah
        $validator = Validator::make($request->all(), [
            'nip' => 'required|unique:axl_master_pegawai,nip,' . $id . ',userid',
            'nama_lengkap' => 'required',
            'email' => 'required|unique:axl_master_pegawai,email,' . $id . ',userid',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $employee->update($request->toArray());
        return response()->json($employee->toArray(), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::destroy($id);
        return response()->json(['userid'=>$id], 204);
    }
}
