# Cara Menjalankan
1. Download
2. Instal dengan perintah `composer install`
3. Set key dengan `php artisan key:generate`
4. Konfigurasi database
5. Jalankan dengan `php artisan serve` (default akan ke localhost:8000)

# File yang perlu diperhatikan
- `app/Http/routes.php` (semua endpoint ada disini)
- `app/Employee.php`
- `app/Http/Controllers/EmployeesController.php`
- `database/seeds/DatabaseSeeder.php` (seeder untuk mengisi sample data)

Catatan khusus:
- menambah auto_increment di field userid
- disable `web` middleware untuk routing (cek `app/Providers/RouteServiceProvider.php` baris 56).
